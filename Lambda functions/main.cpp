#include <cmath>
#include <iostream>
#include <functional>
#include <vector>

using std::vector;
using std::function;
using namespace std;

// This function is a customer filter which uses predicate lambda function

template<typename T, typename FunType>
vector<T> filter(const vector<T> &v, FunType p) {
    std::vector<T> out;
    for (auto &&elem : v)
        if (p(elem)) {
            out.push_back(elem);
        }
    return out;
}

//this function transforms the originally passed vector first, 
//using lambda, then filters it using another lambda (predicate)

template<typename T, typename FunType1, typename FunType2>
vector<T> transfilt(vector<T> &v, FunType1 t, FunType2 p) {
    v = t(v);
    return filter(v, p);
}

//simple vector printing
template<typename T>
void printVec(const vector<T> &v) {
    cout << "[";
    for (int i = 0; i < v.size(); ++i)
        std::cout << v[i] << ' ';
    cout << "]" << endl;
}

int main() {

    vector<int> v{1, -3, 4, -2, 6, -8, 5};
    printVec(v);
    vector<int> r = filter(v, [](int x) { return x % 2 == 0; });
    printVec(r);
    vector<int> s = filter(v, [](int x) { return x >= 0; });
    printVec(s);
    vector<double> w{1.5, -3.1, 4.0, -2.0, 6.3};
    printVec(w);
    double mn = -0.5, mx = 0.5;
    vector<double> d =
            transfilt(w, [](const vector<double> &x) -> vector<double> {
                vector<double> out;
                for (auto &&elem : x) {
                    out.push_back(sin(elem));
                }
                return out;
            }, [=](double x) { return mn <= x && x <= mx; });
    printVec(w);
    printVec(d);

}
