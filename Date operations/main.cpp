//this program's aim is to show bit operations on storing dates.
// it takes two dates in form of integers for year, month and day respectively
// it then packs it into an integer, using bit operations
// and then unpacks it to show the two dates

#include <iostream>
#include <iomanip>

using namespace std;

struct date {
    uint16_t year;
    uint16_t month;
    uint16_t day;
};

uint32_t pack(int fromy, int fromm, int fromd, int toy, int tom, int tod) {

    uint32_t startPeriod = 0;
    uint32_t endPeriod = 0;

    startPeriod |= fromd;
    startPeriod |= fromm << 5;
    fromy -= 2000;
    startPeriod |= fromy << 9;

    endPeriod |= tod;
    endPeriod |= tom << 5;
    toy -= 2000;
    endPeriod |= toy << 9;

    startPeriod <<= 16;
    return startPeriod | endPeriod;
}

date unpack(uint16_t dateUnpack) {
    uint16_t year = (dateUnpack >> 9) + 2000;

    const uint16_t monthBit = 480;
    uint16_t month = (dateUnpack & monthBit) >> 5;

    const uint16_t dayBit = 31;
    uint16_t day = (dateUnpack & dayBit);

    return date{year, month, day};
}

void showDate(uint16_t date) {
    struct date unpackedDate = unpack(date);
    cout
    << unpackedDate.year
    << '/'
    << unpackedDate.month
    << '/'
    << unpackedDate.day;
}

void showPeriod(uint32_t dateUnpack) {
    const uint32_t buffer = 65535;
    uint16_t fromPacked = dateUnpack >> 16;
    uint16_t toPacked = dateUnpack & buffer;

    showDate(fromPacked);
    cout << "-";
    showDate(toPacked);
    cout << endl;
}

int main() {

    uint32_t period = pack(2000, 2, 3, 2127, 11, 29);
    showPeriod(period);
}
