#include <iostream>

using namespace std;

class Segment {
    double A, B;
public:
    Segment(double A, double B) : A(A), B(B) {}

    friend Segment operator+(Segment s1, Segment s2) {
        return {s1.A + s2.A, s1.B + s2.B};
    }

    friend Segment operator+(double d, Segment s) {
        return {d + s.A, d + s.B};
    }

    friend Segment operator+(Segment s, double d) {
        return d + s;
    }

    friend Segment operator*(double d, Segment s) {
        return {s.A * d, s.B * d};
    }

    friend Segment operator*(Segment s, double d) {
        return d * s;
    }

    friend Segment operator/(Segment s, double d) {
        return {s.A / d, s.B / d};
    }

    friend ostream &operator<<(ostream &os, const Segment &segment) {
        os << "A: " << segment.A << " B: " << segment.B;
        return os;
    }

    Segment operator-(double d) const {
        return {A - d, B - d};
    }

    bool operator()(double x) const {
        return x >= A && x <= B;
    }
};


int main() {

    Segment seg{2, 3}, s = 1 + 2 * ((seg - 2) / 2 + seg) / 3;

    auto part0 = (seg - 2) / 2;
    auto part1 = part0 + seg;
    auto part2 = 2 * part1 / 3;
    auto part3 = 1 + part2;


    cout << s << endl << boolalpha;
    for (double x = 0.5; x < 4; x += 1)
        cout << "x=" << x << ": " << s(x) << endl;
}
