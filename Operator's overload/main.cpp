#include <iostream>
#include <cstring>

using namespace std;

class Person {
    char* name;
    friend class Couple;
public:
    explicit Person(const char* n);
    Person(const Person& os);
    Person& operator=(const Person& os);
    ~Person();
    friend ostream& operator<<(ostream& str,
                                    const Person& os);
};

// implementation
class Couple {
    Person *wife, *husband;
public:
    Couple(const Person& she, const Person& he) {
        wife = new Person(she);
        husband = new Person(he);
    }

    Couple(const Couple& other) {
        wife    = new Person(*other.wife);
        husband = new Person(*other.husband);
    }

    Couple& operator=(const Couple& other) {
        if (this == &other) return *this;
        delete wife;
        delete husband;
        wife    = new Person(*other.wife);
        husband = new Person(*other.husband);
        return *this;
    }

    ~Couple() {
        delete wife;
        delete husband;
    }

    friend ostream& operator<<(ostream& str,
                                    const Couple& p);
};

ostream &operator<<(ostream &str, const Couple &p) {
    return str << "Couple: he " << *p.husband << ", she " << *p.wife;
}

Person::Person(const char *n){
    name = new char[strlen(n) + 1];
    strcpy(name, n);
}

Person::Person(const Person &os) {
    name = new char[strlen(os.name) + 1];
    strcpy(name, os.name);
}

Person &Person::operator=(const Person &os) {
    if (this == &os) return *this;
    delete [] name;
    name = new char[strlen(os.name) + 1];
    strcpy(name, os.name);
    return *this;
}

Person::~Person() {
    cout << "DEL " << *this << " " << endl;
    delete [] name;
}

ostream &operator<<(ostream &str, const Person &os) {
    return str << os.name;
}
// implementation

int main(void) {
    Person *pjohn = new Person("John"),
            *pjane = new Person("Jane");
    Person mary("Mary"), mark("Mark");
    Couple *pcouple1 = new Couple(mary, *pjohn);
    Couple couple2(*pjane,mark);
    delete pjohn;
    delete pjane;
    cout << *pcouple1 << endl;
    cout << couple2 << endl;
    couple2 = *pcouple1;
    delete pcouple1;
    cout << couple2 << endl;
}
