This project contains short, usually one-function algorithms for solving certain problems.

As I'm a beginner in C++, this project's aim is to showcase some algorithmic skills
more than best practices of coding in C++.
