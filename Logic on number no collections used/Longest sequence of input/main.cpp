#include <iostream>

using namespace std;

int main() {

    int longest = 0;
    int longest_count = 1;
    int current;
    int current_count = 0;
    int sequence;

    while(true) {

        cout << "Value: ";
        cin >> current;

        if (current == 0) {
            //exit
            cout << "Longest sequence: " << longest_count << " times " << longest << endl;
            return 0;
        }

        if (current != sequence && current == longest) {
            longest_count = 0;
            longest_count++;
            sequence = current;
        }

        if(current == sequence && current == longest) {
            longest_count++;
        }

        if (current != longest) {
            if (current == sequence) {
                current_count++;
            } else {
                current_count = 0;
                current_count++;
                sequence = current;
            }
        }

        if (current_count > longest_count) {
            longest = current;
            longest_count = current_count;
            current_count = 0;
        }
    }
}
