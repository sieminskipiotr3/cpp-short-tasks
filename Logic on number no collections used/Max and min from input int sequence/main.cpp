#include <iostream>
#include <limits.h>
#pragma once

using namespace std;

int main()
{
    cout << "Please enter an integer. when 0 is entered, program will stop" << endl;

    int max = INT_MIN;
    int max_count = 0;
    int min = INT_MAX;
    int min_count = 0;

    while(true)
    {
        int current = 0;
        cout << "Value:";
        cin >> current;

        if(current == 0)
        {
            //exit
            cout << "Min = "  << min  << " " << min_count  << " times" << endl;
            cout << "Max = "  << max  << " " << max_count  << " times" << endl;
            return 0;
        }


        if(current > max) {
            max = current;
            max_count = 0;
        }

        if(current < min) {
            min = current;
            min_count = 0;
        }

        if(current == max) {
            max_count++;
        }
        if (current == min) {
            min_count++;
        }

    }
}

